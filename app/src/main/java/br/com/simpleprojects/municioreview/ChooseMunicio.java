package br.com.simpleprojects.municioreview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ChooseMunicio extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_municio);
    }

    public void toFrance(View view){
        Intent intent = new Intent(this, MunicioA.class);
        startActivity(intent);
    }
    public void toSiria(View view){
        Intent intent = new Intent(this, municioSirio.class);
        startActivity(intent);
    }
    public void toAustralia(View view){
        Intent intent = new Intent(this, MunicioAustraliano.class);
        startActivity(intent);
    }
}
